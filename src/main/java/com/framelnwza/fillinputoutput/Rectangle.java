/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.fillinputoutput;

import java.io.Serializable;

/**
 *
 * @author Gigabyte
 */
public class Rectangle implements Serializable{
    private int width;
    private int hight;

    public Rectangle(int width, int hight) {
        this.width = width;
        this.hight = hight;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHight() {
        return hight;
    }

    public void setHight(int hight) {
        this.hight = hight;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "width=" + width + ", hight=" + hight + '}';
    }
    
    
}
