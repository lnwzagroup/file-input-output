/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.fillinputoutput;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author Gigabyte
 */
public class RSPgame extends javax.swing.JFrame {

    /**
     * Creates new form RSPgame
     */
   public RSPgame() {
        initComponents();
        ImageIcon imageRock = new ImageIcon("picture/ROCK.png");
        btnRock.setIcon(imageRock);
        ImageIcon imageScissors = new ImageIcon("picture/SCISSORS.png");
        btnScissors.setIcon(imageScissors);
        ImageIcon imagePaper = new ImageIcon("picture/PAPER.png");
        btnPaper.setIcon(imagePaper);
       Botprogram botprogram = new Botprogram();
        load();
        showState();
    }
   private void initComponents() {

       Component lblState = new javax.swing.JLabel();
       Component jLabel1 = new javax.swing.JLabel();
       Component jLabel2 = new javax.swing.JLabel();
       Component lblPlayer = new javax.swing.JLabel();
       Component lblResult = new javax.swing.JLabel();
       Component lblBot = new javax.swing.JLabel();
       Component btnScissors = new javax.swing.JButton();
       Component btnRock = new javax.swing.JButton();
       JButton btnPaper = new javax.swing.JButton();
       Component btnReset = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(182, 182));

        lblState.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblState.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblState.setText("Win: 0 Draw:0 Lose:0");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Bot");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Player");

        lblPlayer.setBackground(new java.awt.Color(255, 255, 255));
        lblPlayer.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblPlayer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPlayer.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        lblPlayer.setOpaque(true);

        lblResult.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblResult.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblResult.setText("?");

        lblBot.setBackground(new java.awt.Color(255, 255, 255));
        lblBot.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblBot.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblBot.setOpaque(true);

        btnScissors.setBackground(new java.awt.Color(255, 255, 255));
        btnScissors.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnScissors.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnScissorsActionPerformed(evt);
            }

           private void btnScissorsActionPerformed(ActionEvent evt) {
               throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }
        });

        btnRock.setBackground(new java.awt.Color(255, 255, 255));
        btnRock.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnRock.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnRock.setDoubleBuffered(true);
        btnRock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRockActionPerformed(evt);
            }

           private void btnRockActionPerformed(ActionEvent evt) {
               throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }
        });

        btnPaper.setBackground(new java.awt.Color(255, 255, 255));
        btnPaper.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPaper.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaperActionPerformed(evt);
            }

           private void btnPaperActionPerformed(ActionEvent evt) {
               throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }
        });

        btnReset.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnReset.setText("Reset");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }

           private void btnResetActionPerformed(ActionEvent evt) {
               throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblResult, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblState, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                            .addComponent(lblBot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPlayer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnRock, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                        .addComponent(btnScissors, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(btnPaper, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(125, 125, 125))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblState)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblPlayer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblBot, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(lblResult)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnScissors, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnRock, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnPaper, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }

    private void btnRockActionPerformed(java.awt.event.ActionEvent evt) {                                        
        int status = botprogram.ChoobChoob(0);
        showBot();
        showPlayer();
        showResult();
        showState();
        save();
    }     
    private void btnScissorsActionPerformed(java.awt.event.ActionEvent evt) {                                            
        int status = botprogram.ChoobChoob(1);
        showBot();
        showPlayer();
        showResult();
        showState();
        save();
    }        
     private void btnPaperActionPerformed(java.awt.event.ActionEvent evt) {                                         
        int status = botprogram.ChoobChoob(2);
        showBot();
        showPlayer();
        showResult();
        showState();
        save();
    }
      private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {                                         
        botprogram= new Botprogram();
        save();
        showState();
    }                                        
    private String[] strRSP = {"Rock", "Scissors", "Paper"};

    private void showPlayer() {
        //lblPlayer.setText(strRSP[botprogram.getPlayerHand()]);
        ImageIcon image = new ImageIcon("picture/" + strRSP[botprogram.getPlayerHand()] + ".png");
        lblPlayer.setIcon(image);
    }

    private void showBot() {
        // lblBot.setText(strRSP[botprogram.getHand()]);
        ImageIcon image = new ImageIcon("picture/" + strRSP[botprogram.getHand()] + ".png");
        lblBot.setIcon(image);
    }

    private void showResult() {
        if (botprogram.getStatus() == 0) {
            lblResult.setText("Draw!!!");
        } else if (botprogram.getStatus() == 1) {
            lblResult.setText("Win!!!");
        } else {
            lblResult.setText("Lose!!!");
        }
    }

    private void showState() {
        lblState.setText("Win: " + botprogram.getWin() + "Draw: "
                + "" + botprogram.getDraw() + "Lose: " + botprogram.getLose());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
     try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RSPgame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RSPgame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RSPgame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RSPgame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RSPgame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RSPgame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RSPgame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RSPgame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RSPgame().setVisible(true);
            }
        });
    }

  private void save() {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            File file = new File("computer.obj");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(botprogram);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RSPgame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RSPgame.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(RSPgame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private void load() {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        File file = new File("computer.obj");
        fis = new FileInputStream(file);
        ois = new ObjectInputStream(fis);
        botprogram = (Botprogram) ois.readObject();
        ois.close();
        fis.close();
        try {
            if (fis != null) {
                fis.close();
            }
        } catch (NullPointerException ex) {
            System.out.println("Null poinet exception!!!");
        }
    }
    private Botprogram botprogram;
    // Variables declaration - do not modify                     
    private javax.swing.JButton btnPaper;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnRock;
    private javax.swing.JButton btnScissors;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblBot;
    private javax.swing.JLabel lblPlayer;
    private javax.swing.JLabel lblResult;
    private javax.swing.JLabel lblState;
    // End of variables declaration                   
}
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
