/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.fillinputoutput;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Gigabyte
 */
public class Botprogram implements Serializable{
    private int Hand;
    private int PlayerHand;
    private int win,lose,draw;
    private int status;
    
    public Botprogram(){
        
    }
    private int choob(){
       return ThreadLocalRandom.current().nextInt(0,3);
    }
    
    public int ChoobChoob(int PlayerHand){
        this.PlayerHand=PlayerHand;
        this.Hand=choob();
        if(this.PlayerHand==this.Hand){
            draw++;
            status=0;
            return 0;
        }
        if(this.PlayerHand==0&&this.Hand==1){
            win++;
            status=1;
            return 1;
        }
        if(this.PlayerHand==1&&this.Hand==2){
            win++;
            status=1;
            return 1;
        }
        if(this.PlayerHand==2&&this.Hand==0){
            win++;
            status=1;
            return 1;
        }
        lose++;
        status=-1;
        return -1;
    }

    public int getHand() {
        return Hand;
    }

    public int getPlayerHand() {
        return PlayerHand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }
    
}